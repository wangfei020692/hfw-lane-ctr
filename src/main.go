package main

import (
	"flag"
	"fmt"
	"hfwLaneCtr/handler/nanjing"
	"hfwLaneCtr/res"
	"log"
	"net/http"

	"github.com/cihub/seelog"
	"github.com/gorilla/mux"
)

func main() {
	logger, err := seelog.LoggerFromConfigAsFile("logconfig.xml")
	if err != nil {
		log.Fatalf("Unable to create logger: %s", err)
	}
	seelog.ReplaceLogger(logger)

	StartHttpServer(res.Config.Addr, res.Config.HttpPort)
}

func StartHttpServer(strip string, port int) {
	r := mux.NewRouter()

	//南京
	//设备驱动服务初始化（设备驱动服务接口）
	r.HandleFunc("/service", nanjing.NjSoftwareServerProxy.SoftwareInit).Methods("POST")

	//天线 1开启 0关闭
	if res.Config.NjRsuFlag == 1 {

	}
	//移动支付 1开启 0关闭
	if res.Config.NjMpayFlag == 1 {

	}
	//称台 1开启 0关闭
	if res.Config.NjWeightFlag == 1 {

	}
	//读写器 1开启 0关闭
	if res.Config.NjReaderFlag == 1 {

	}
	//栏杆机 1开启 0关闭
	if res.Config.NjBlusterFlag == 1 {

	}
	//车牌识别 1开启 0关闭
	if res.Config.NjVprFlag == 1 {
		r.HandleFunc("/device/vpr", nanjing.NjVprServerProxy.VprApi).Methods("POST")

		r.HandleFunc("/vlpr/reginfo", nanjing.NjVprServerProxy.VprDeviceReginfo).Methods("POST")
	}
	//车型识别 1开启 0关闭
	if res.Config.NjVlprFlag == 1 {
		r.HandleFunc("/device/vlpr", nanjing.NjVprServerProxy.VlprApi).Methods("POST")
	}
	//自助机器人 1开启 0关闭
	if res.Config.NjCardRobotFlag == 1 {

	}
	//打印机 1开启 0关闭
	if res.Config.NjPrinterFlag == 1 {

	}
	//数字经济大屏 1开启 0关闭
	if res.Config.NjDigitalScreenFlag == 1 {

	}
	//驱动服务 1开启 0关闭
	if res.Config.NjDriverFlag == 1 {

	}

	//url文件
	// var dir string
	// flag.StringVar(&dir, "dir", "./web", "the directory to serve files from. Defaults to the current dir")
	// flag.Parse()
	// r.PathPrefix("/web/").Handler(http.StripPrefix("/web/", http.FileServer(http.Dir(dir))))

	//url文件
	// var dir string
	// flag.StringVar(&dir, "dir", "./web", "the directory to serve files from. Defaults to the current dir")
	// flag.Parse()
	// r.PathPrefix("/web/").Handler(http.StripPrefix("/web/", http.FileServer(http.Dir(dir))))

	var dir1 string
	flag.StringVar(&dir1, "dir1", "./logs", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()
	r.PathPrefix("/logs/").Handler(http.StripPrefix("/logs/", http.FileServer(http.Dir(dir1))))

	strAddr := fmt.Sprintf("%s:%d", strip, port)

	seelog.Infof("http %s Listening...", strAddr)
	fmt.Println("http ", strAddr, " Listening...")

	//go nanjing.NjSoftwareServer.SendDeviceStatus()
	//go nanjing.NjSoftwareServer.UpdateSystemTime()

	log.Fatal(http.ListenAndServe(strAddr, r))

	return
}

package util

var (
	//请求返回状态
	ResultSuccInfo = &Err{0, "成功"}
	ResultFailInfo = &Err{1, "失败"}
	//设备状态码
	DeviceSuccInfo   = &Err{0, "正常"}
	DeviceFailInfo   = &Err{1, "故障"}
	DeviceFailInfo99 = &Err{99, "故障"}

	SystemError = &Err{-1, "系统错误"}
)

type Err struct {
	Code int32
	Msg  string
}

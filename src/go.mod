module hfwLaneCtr

go 1.19

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/gorilla/mux v1.8.0
)

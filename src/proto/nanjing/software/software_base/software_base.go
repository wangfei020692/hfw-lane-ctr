package software_base

type SoftwareInitQ struct {
	AppUrl        string `json:"appUrl"`        //车道应用所有接口前缀
	StateInterval int32  `json:"stateInterval"` //车道设备状态上传时间间隔，默认6s
	NtpServer     string `json:"ntpServer"`     //ntp服务器ip地址
}

type SoftwareInitP struct {
	Code    int32    `json:"code"`    //响应结果码
	Msg     string   `json:"msg"`     //响应消息内容
	Devices []string `json:"devices"` //车道驱动服务注册设备
}

type LabelStatus struct {
	Role int32  `json:"role"` //所有设备都使用该字段，默认为0，表示默认设备，其余role参考具体设备role表
	Code int32  `json:"code"` //设备状态码 见附表
	Msg  string `json:"msg"`  //设备状态描述
}

type UploadState struct {
	Rsu           []*LabelStatus `json:"rsu"`           //1种设备
	Mpay          []*LabelStatus `json:"mpay"`          //1种设备
	Weight        []*LabelStatus `json:"weight"`        //1种设备
	Reader        []*LabelStatus `json:"reader"`        //3种设备333
	Bluster       []*LabelStatus `json:"bluster"`       //1种设备
	Vpr           []*LabelStatus `json:"vpr"`           //1种设备
	Vlpr          []*LabelStatus `json:"vlpr"`          //2种设备222
	CardRobot     []*LabelStatus `json:"cardRobot"`     //1种设备
	Printer       []*LabelStatus `json:"printer"`       //1种设备
	DigitalScreen []*LabelStatus `json:"digitalScreen"` //1种设备
	Driver        []*LabelStatus `json:"driver"`        //1种设备
}

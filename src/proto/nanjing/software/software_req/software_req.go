package software_req

import (
	"hfwLaneCtr/proto/nanjing/software/software_base"
)

type SoftwareInitReq struct {
	Id   int64                       `json:"id"`   //本次请求id
	Opt  int32                       `json:"opt"`  //操作项，初始化为1
	Data software_base.SoftwareInitQ `json:"data"` //请求相关数据
}

type UploadStateReq struct {
	Id   int64                     `json:"id"`   //本次请求id
	Data software_base.UploadState `json:"data"` //请求相关数据
}

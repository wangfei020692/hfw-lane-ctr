package software_resp

import (
	"hfwLaneCtr/proto/nanjing/software/software_base"
)

type SoftwareInitResp struct {
	Id   int64                       `json:"id"`   //本次请求id
	Opt  int32                       `json:"opt"`  //操作项，初始化为1
	Data software_base.SoftwareInitP `json:"data"` //请求相关数据
}

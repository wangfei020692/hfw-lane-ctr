package vlpr_resp

import (
	"hfwLaneCtr/proto/nanjing/vlpr/vlpr_base"
)

type VlprApiResp struct {
	Id   int64              `json:"id"`   //本次请求id
	Opt  int32              `json:"opt"`  //操作项，初始化为1
	Data vlpr_base.VlprApiP `json:"data"` //请求相关数据
}

type VlprDeviceInitResp struct {
	ReceiveTime string `json:"receiveTime"` //服务端接收时间
	SubCode     int32  `json:"subCode"`     //错误码
	ErrorMsg    string `json:"errorMsg"`    //错误描述
}

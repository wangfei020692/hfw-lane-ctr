package vlpr_base

type VprApiQ struct {
	Role   int32  `json:"role"`   //4.2.1连接（设备驱动服务接口）------0车道口首个车型识别设备 1车道口第二个车型识别设备
	DataId string `json:"dataId"` //4.2.4获取车辆图片、视频（设备驱动服务接口）------文件数据id 同一车辆信息dataId 和车辆信息上报中 dataId 相同
}

type VlprApiP struct {
	Code int32  `json:"code"` //响应结果码
	Msg  string `json:"msg"`  //响应消息内容
}

type VlprResultConnect struct {
	Code int32  `json:"code"` //响应结果码
	Msg  string `json:"msg"`  //响应消息内容
}

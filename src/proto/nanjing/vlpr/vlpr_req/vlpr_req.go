package vlpr_req

import (
	"hfwLaneCtr/proto/nanjing/vlpr/vlpr_base"
)

type VlprApiReq struct {
	Id   int64             `json:"id"`   //本次请求id
	Opt  int32             `json:"opt"`  //操作项，初始化为1
	Data vlpr_base.VprApiQ `json:"data"` //请求相关数据
}

type VlprDeviceInitReq struct {
	DevIP    string `json:"devIP"`    //车道机IP地址
	DevPort  int32  `json:"devPort"`  //车道机监听端口
	User     string `json:"user"`     //登录用户名
	Passwd   string `json:"passwd"`   //用户密码
	Interval int32  `json:"interval"` //设备状态上报状态频率
	Switch   int32  `json:"switch"`   //上报接口开关
}

type VlprResultConnectReq struct {
	Id   int64                       `json:"id"`   //本次请求id
	Opt  int32                       `json:"opt"`  //操作项，初始化为1
	Data vlpr_base.VlprResultConnect `json:"data"` //请求相关数据
}

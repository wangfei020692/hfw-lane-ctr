package vpr_req

import "hfwLaneCtr/proto/nanjing/vpr/vpr_base"

type VprApiReq struct {
	Id   int64                 `json:"id"`   //本次请求id
	Opt  int32                 `json:"opt"`  //操作项，初始化为1
	Data vpr_base.VprVideoInfo `json:"data"` //请求相关数据
}

type VprDeviceInitReq struct {
	DevIP    string `json:"devIP"`    //车道机IP地址
	DevPort  int32  `json:"devPort"`  //车道机监听端口
	User     string `json:"user"`     //登录用户名
	Passwd   string `json:"passwd"`   //用户密码
	Interval int32  `json:"interval"` //设备状态上报状态频率
}

type VprResultConnectReq struct {
	Id   int64                     `json:"id"`   //本次请求id
	Opt  int32                     `json:"opt"`  //操作项，初始化为1
	Data vpr_base.VprResultConnect `json:"data"` //请求相关数据
}

type VprDeviceCaptureReq struct {
	TriggerSign int64 `json:"triggerSign"` //时间戳
}

type VprResultCaptureReq struct {
	Id   int64                     `json:"id"`   //本次请求id
	Opt  int32                     `json:"opt"`  //操作项，初始化为1
	Data vpr_base.VprResultCapture `json:"data"` //请求相关数据
}

type VprDeviceReginfoReq struct {
	RegTime          string `json:"regTime"`          //识别时间
	PlateNumber      string `json:"plateNumber"`      //车牌号码
	PlateColor       int32  `json:"plateColor"`       //车牌颜色
	ImageSize        int32  `json:"imageSize"`        //抓拍图大小
	LicenselmageSize int32  `json:"licenselmageSize"` //车牌图大小
	BinImageSize     int32  `json:"binImageSize"`     //二值化图大小
	ImageData        string `json:"imageData"`        //抓拍图片数据
	LicenseImageData string `json:"licenseImageData"` //车牌图片数据
	BinImageData     string `json:"binImageData"`     //二值化图片
	TriggerSign      int32  `json:"triggerSign"`      //触发标记
}

type VprUploadCarPlateReq struct {
	Id   int64                      `json:"id"`   //本次请求id
	Opt  int32                      `json:"opt"`  //操作项，初始化为1
	Data vpr_base.VprUploadCarPlate `json:"data"` //请求相关数据
}

package vpr_base

type VprVideoInfo struct {
	FileName   string `json:"fileName"`   //录像文件保存名称
	RecordTime int32  `json:"recordTime"` //录像时长 单位：毫秒
}

type VprApiP struct {
	Code int32  `json:"code"` //响应结果码
	Msg  string `json:"msg"`  //响应消息内容
}

type VprResultConnect struct {
	Code int32  `json:"code"` //响应结果码
	Msg  string `json:"msg"`  //响应消息内容
}

type VprResultCapture struct {
	Code int32  `json:"code"` //响应结果码
	Msg  string `json:"msg"`  //响应消息内容
}

type VprUploadCarPlate struct {
	VehPlate   string `json:"vehPlate"`   //车牌数据
	PlateColor int32  `json:"plateColor"` //车牌颜色
	BinImgLen  int32  `json:"binImgLen"`  //二制化图片长度（加密前的数据）
	BinImg     string `json:"binImg"`     //二制化图片数据 （base64加密）
	ImgLen     int32  `json:"imgLen"`     //抓拍图片长度
	ImgData    string `json:"imgData"`    //抓拍图片数据
}

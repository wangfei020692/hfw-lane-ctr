package res

import (
	"fmt"
	"hfwLaneCtr/nf/util/ini"
	"strconv"
	"sync"
)

var (
	mutex  sync.Mutex
	Config *config
)

func initNConf() {
	mutex.Lock()
	defer mutex.Unlock()

	file, err := ini.LoadFile("nconf.ini")

	if err != nil {
		fmt.Println("initNConf error")
		panic(err)
	}

	Config = &config{}
	serverSec := file.Section("server")

	Config.Addr = serverSec["Addr"]
	strHttpPort := serverSec["HttpPort"]
	Config.HttpPort, err = strconv.Atoi(strHttpPort)
	if err != nil {
		panic(err)
	}

	//天线 1开启 0关闭
	NjRsuFlag := serverSec["NjRsuFlag"]
	Config.NjRsuFlag, err = strconv.Atoi(NjRsuFlag)
	if err != nil {
		panic(err)
	}
	//移动支付 1开启 0关闭
	NjMpayFlag := serverSec["NjMpayFlag"]
	Config.NjMpayFlag, err = strconv.Atoi(NjMpayFlag)
	if err != nil {
		panic(err)
	}
	//称台 1开启 0关闭
	NjWeightFlag := serverSec["NjWeightFlag"]
	Config.NjWeightFlag, err = strconv.Atoi(NjWeightFlag)
	if err != nil {
		panic(err)
	}
	//读写器 1开启 0关闭
	NjReaderFlag := serverSec["NjReaderFlag"]
	Config.NjReaderFlag, err = strconv.Atoi(NjReaderFlag)
	if err != nil {
		panic(err)
	}
	//栏杆机 1开启 0关闭
	NjBlusterFlag := serverSec["NjBlusterFlag"]
	Config.NjBlusterFlag, err = strconv.Atoi(NjBlusterFlag)
	if err != nil {
		panic(err)
	}
	//车牌识别 1开启 0关闭
	NjVprFlag := serverSec["NjVprFlag"]
	Config.NjVprFlag, err = strconv.Atoi(NjVprFlag)
	if err != nil {
		panic(err)
	}
	Config.StrNjVprAddr = serverSec["NjVprAddr"]
	Config.StrNjVprUserName = serverSec["NjVprUserName"]
	Config.StrNjVprPassWord = serverSec["NjVprPassWord"]
	//车型识别 1开启 0关闭
	NjVlprFlag := serverSec["NjVlprFlag"]
	Config.NjVlprFlag, err = strconv.Atoi(NjVlprFlag)
	if err != nil {
		panic(err)
	}
	Config.StrNjVlprAddr = serverSec["NjVlprAddr"]
	Config.StrNjVlprUserName = serverSec["NjVlprUserName"]
	Config.StrNjVlprPassWord = serverSec["NjVlprPassWord"]
	//自助机器人 1开启 0关闭
	NjCardRobotFlag := serverSec["NjCardRobotFlag"]
	Config.NjCardRobotFlag, err = strconv.Atoi(NjCardRobotFlag)
	if err != nil {
		panic(err)
	}
	//打印机 1开启 0关闭
	NjPrinterFlag := serverSec["NjPrinterFlag"]
	Config.NjPrinterFlag, err = strconv.Atoi(NjPrinterFlag)
	if err != nil {
		panic(err)
	}
	//数字经济大屏 1开启 0关闭
	NjDigitalScreenFlag := serverSec["NjDigitalScreenFlag"]
	Config.NjDigitalScreenFlag, err = strconv.Atoi(NjDigitalScreenFlag)
	if err != nil {
		panic(err)
	}
	//驱动服务 1开启 0关闭
	NjDriverFlag := serverSec["NjDriverFlag"]
	Config.NjDriverFlag, err = strconv.Atoi(NjDriverFlag)
	if err != nil {
		panic(err)
	}
}

type config struct {
	Addr                string
	HttpPort            int
	NjRsuFlag           int //天线 1开启 0关闭
	NjMpayFlag          int //移动支付 1开启 0关闭
	NjWeightFlag        int //称台 1开启 0关闭
	NjReaderFlag        int //读写器 1开启 0关闭
	NjBlusterFlag       int //栏杆机 1开启 0关闭
	NjVprFlag           int //车牌识别 1开启 0关闭
	StrNjVprAddr        string
	StrNjVprUserName    string
	StrNjVprPassWord    string
	NjVlprFlag          int //车型识别 1开启 0关闭
	StrNjVlprAddr       string
	StrNjVlprUserName   string
	StrNjVlprPassWord   string
	NjCardRobotFlag     int //自助机器人 1开启 0关闭
	NjPrinterFlag       int //打印机 1开启 0关闭
	NjDigitalScreenFlag int //数字经济大屏 1开启 0关闭
	NjDriverFlag        int //驱动服务 1开启 0关闭
}

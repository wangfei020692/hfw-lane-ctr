package nanjing

import (
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
	_req "hfwLaneCtr/proto/nanjing/vpr/vpr_req"
	_resp "hfwLaneCtr/proto/nanjing/vpr/vpr_resp"
	"hfwLaneCtr/res"
	"hfwLaneCtr/util"
	"time"
)

var (
	NjVprServer = &njVprServer{}
)

type njVprServer struct {
}

const (
	g_VprOpt_1 = 0x01 //连接
	g_VprOpt_2 = 0x02 //手动抓拍
	g_VprOpt_3 = 0x03 //开始录像
	g_VprOpt_4 = 0x04 //车牌数据上报
)

func (srv *njVprServer) VprApi(req *_req.VprApiReq) *_resp.VprApiResp {
	//构造返回
	resp := &_resp.VprApiResp{}
	resp.Id = req.Id
	resp.Opt = req.Opt
	resp.Data.Code = util.ResultSuccInfo.Code
	resp.Data.Msg = util.ResultSuccInfo.Msg

	switch req.Opt {
	case g_VprOpt_1: //连接
		go srv.VprResultConnect()
		break
	case g_VprOpt_2: //手动抓拍
		go srv.VprResultCapture()
		break
	case g_VprOpt_3: //开始录像
		//未找到对应文档无法对应-对于req需要校验入参
		break
	}

	return resp
}

func (srv *njVprServer) VprResultCapture() {
	//参数名ASCII码从小到大排序（字典序）,*******在赋值的时间代码就要写好顺序*******
	devReq := &_req.VprDeviceCaptureReq{}
	i64NowTime := time.Now().Unix()
	devReq.TriggerSign = i64NowTime

	reqJson, _ := json.Marshal(devReq)
	//创造signValue
	strSignValue := fmt.Sprintf("TriggerSign=%d", i64NowTime)
	strSignValue += "&filename=capture.json"

	strDeviceResp := HttpClientPost2DeviceVprVlpr(res.Config.StrNjVprAddr, "/vlpr/capture", string(reqJson), strSignValue, "capture")
	if strDeviceResp == "error" {
		return
	}
	//需要做返回
	var deviceResp *_resp.VprDeviceCaptureResp
	err := json.Unmarshal([]byte(strDeviceResp), &deviceResp)
	if err != nil {
		fmt.Println("南京 VprResultCapture VprDeviceCaptureResp rev json unmarshal failed")
		log.Infof("南京 VprResultCapture VprDeviceCaptureResp rev json unmarshal failed")
		return
	}

	if deviceResp.SubCode == 0 { //成功
		reqServer := &_req.VprResultCaptureReq{}
		reqServer.Id = GetSendDataId()
		reqServer.Opt = g_VprOpt_2
		reqServer.Data.Code = util.ResultSuccInfo.Code
		reqServer.Data.Msg = util.ResultSuccInfo.Msg

		reqJson, _ := json.Marshal(reqServer)

		strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vpr", string(reqJson))
		if strResp == "error" {
			log.Info("VprResultCapture HttpClientPost2Server send err")
		}
	} else { //失败
		reqServer := &_req.VprResultCaptureReq{}
		reqServer.Id = GetSendDataId()
		reqServer.Opt = g_VprOpt_2
		reqServer.Data.Code = util.ResultFailInfo.Code
		reqServer.Data.Msg = util.ResultFailInfo.Msg

		reqJson, _ := json.Marshal(reqServer)

		strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vpr", string(reqJson))
		if strResp == "error" {
			log.Info("VprResultCapture HttpClientPost2Server send err")
		}
	}
}

func (srv *njVprServer) VprResultConnect() {
	//参数名ASCII码从小到大排序（字典序）,*******在赋值的时间代码就要写好顺序*******
	devReq := &_req.VprDeviceInitReq{}
	devReq.DevIP = res.Config.Addr              //必填
	devReq.DevPort = int32(res.Config.HttpPort) //必填
	devReq.Interval = 300                       //非必填
	devReq.Passwd = res.Config.StrNjVprPassWord //非必填
	devReq.User = res.Config.StrNjVprUserName   //非必填
	reqJson, _ := json.Marshal(devReq)

	//创造signValue
	strSignValue := fmt.Sprintf("devIP=%s&devPort=%d", devReq.DevIP, devReq.DevPort)
	if devReq.Interval != 0 {
		strSignValue += fmt.Sprintf("&interval=%d", devReq.Interval)
	}
	if devReq.Passwd != "" {
		strSignValue += fmt.Sprintf("&passwd=%s", devReq.Passwd)
	}
	if devReq.User != "" {
		strSignValue += fmt.Sprintf("&user=%s", devReq.User)
	}
	strSignValue += "&filename=init.json"

	strDeviceResp := HttpClientPost2DeviceVprVlpr(res.Config.StrNjVprAddr, "/vlpr/init", string(reqJson), strSignValue, "init")
	if strDeviceResp == "error" {
		return
	}

	//需要做返回
	var deviceResp *_resp.VprDeviceInitResp
	err := json.Unmarshal([]byte(strDeviceResp), &deviceResp)
	if err != nil {
		fmt.Println("南京 VprResultConnect VprDeviceInitResp rev json unmarshal failed")
		log.Infof("南京 VprResultConnect VprDeviceInitResp rev json unmarshal failed")
		return
	}

	if deviceResp.SubCode == 0 { //成功
		NjSoftwareServer.G_NjVprConnStatus0 = 1 //状态上报赋值

		reqServer := &_req.VprResultConnectReq{}
		reqServer.Id = GetSendDataId()
		reqServer.Opt = g_VprOpt_1
		reqServer.Data.Code = util.ResultSuccInfo.Code
		reqServer.Data.Msg = util.ResultSuccInfo.Msg

		reqJson, _ := json.Marshal(reqServer)

		strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vpr", string(reqJson))
		if strResp == "error" {
			log.Info("VprResultConnect HttpClientPost2Server send err")
		}
	} else { //失败
		NjSoftwareServer.G_NjVprConnStatus0 = 0 //状态上报赋值

		reqServer := &_req.VprResultConnectReq{}
		reqServer.Id = GetSendDataId()
		reqServer.Opt = g_VprOpt_1
		reqServer.Data.Code = util.ResultFailInfo.Code
		reqServer.Data.Msg = util.ResultFailInfo.Msg

		reqJson, _ := json.Marshal(reqServer)

		strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vpr", string(reqJson))
		if strResp == "error" {
			log.Info("VprResultConnect HttpClientPost2Server send err")
		}
	}
}

func (srv *njVprServer) VprDeviceReginfo(req *_req.VprDeviceReginfoReq) *_resp.VprDeviceReginfoResp {
	reqServer := &_req.VprUploadCarPlateReq{}
	reqServer.Id = GetSendDataId()
	reqServer.Opt = g_VprOpt_4
	reqServer.Data.VehPlate = req.PlateNumber
	if req.PlateColor >= 0 && req.PlateColor <= 6 {
		reqServer.Data.PlateColor = req.PlateColor
	} else {
		reqServer.Data.PlateColor = 9
	}
	reqServer.Data.BinImgLen = req.BinImageSize
	reqServer.Data.BinImg = req.BinImageData
	reqServer.Data.ImgLen = req.ImageSize
	reqServer.Data.BinImg = req.ImageData

	reqJson, _ := json.Marshal(req)

	strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vpr", string(reqJson))

	resp := &_resp.VprDeviceReginfoResp{}
	if strResp == "error" {
		log.Info("VprDeviceReginfo VprDeviceReginfoResp send err")

		resp.ReceiveTime = time.Now().Format("2006-01-02 15:04:05")
		resp.ErrorMsg = util.DeviceFailInfo99.Msg
		resp.SubCode = util.DeviceFailInfo99.Code

		return resp
	}

	resp.ReceiveTime = time.Now().Format("2006-01-02 15:04:05")
	resp.ErrorMsg = util.ResultSuccInfo.Msg
	resp.SubCode = util.ResultSuccInfo.Code

	return resp
}

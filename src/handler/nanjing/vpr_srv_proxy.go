package nanjing

import (
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
	_req "hfwLaneCtr/proto/nanjing/vpr/vpr_req"
	_resp "hfwLaneCtr/proto/nanjing/vpr/vpr_resp"
	"hfwLaneCtr/util"
	"io/ioutil"
	"net/http"
	"runtime/debug"
	"time"
)

var (
	NjVprServerProxy = &njVprServerProxy{}
)

type njVprServerProxy struct {
}

func (srv *njVprServerProxy) VprApi(w http.ResponseWriter, r *http.Request) {
	fmt.Println("接口 南京 VprApi 收到数据") //======================
	log.Infof("接口 南京 VprApi 收到数据")   //======================

	w.Header().Set("Access-Control-Allow-Origin", "*")                                          //允许访问所有域
	w.Header().Add("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-csrftoken") //header的类型 返回数据格式是json
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Max-Age", "0")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("content-type", "application/json;charset=UTF-8")

	defer r.Body.Close()

	var req *_req.VprApiReq //======================
	var err error

	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &req)

	if err != nil {
		fmt.Println("南京 VprApi rev json unmarshal failed") //======================
		log.Infof("南京 VprApi rev json unmarshal failed")   //======================

		TmpResp := &_resp.VprApiResp{} //======================

		TmpResp.Id = GetSendDataId()
		TmpResp.Opt = 1
		TmpResp.Data.Code = util.ResultFailInfo.Code
		TmpResp.Data.Msg = util.ResultFailInfo.Msg

		TmpRespJson, TmpRespRespjsonErr := json.Marshal(TmpResp)
		if TmpRespRespjsonErr != nil {
			log.Infof("南京 VprApi TmpResp send json unmarshal failed")   //======================
			fmt.Println("南京 VprApi TmpResp send json unmarshal failed") //======================
		}
		w.WriteHeader(http.StatusOK)
		w.Write(TmpRespJson)

		return
	}

	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", string(body), e, debug.Stack())
		}
	}()

	fmt.Println("收到http数据：", string(body))
	log.Infof("收到http数据：%v", string(body))

	resp := NjVprServer.VprApi(req) //======================

	respJson, jsonErr := json.Marshal(resp)
	if jsonErr != nil {
		fmt.Println("南京 VprApi send json unmarshal failed") //======================
		log.Infof("南京 VprApi send json unmarshal failed")   //======================
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("发送http数据：", string(respJson))
	log.Infof("发送http数据：%v", string(respJson))

	w.WriteHeader(http.StatusOK)
	w.Write(respJson)

	return
}

func (srv *njVprServerProxy) VprDeviceReginfo(w http.ResponseWriter, r *http.Request) {
	fmt.Println("接口 南京 VprDeviceReginfo 收到数据") //======================
	log.Infof("接口 南京 VprDeviceReginfo 收到数据")   //======================

	w.Header().Set("Access-Control-Allow-Origin", "*")                                          //允许访问所有域
	w.Header().Add("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-csrftoken") //header的类型 返回数据格式是json
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Max-Age", "0")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("content-type", "application/json;charset=UTF-8")

	defer r.Body.Close()

	var req *_req.VprDeviceReginfoReq //======================
	var err error

	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &req)

	if err != nil {
		fmt.Println("南京 VprDeviceReginfo rev json unmarshal failed") //======================
		log.Infof("南京 VprDeviceReginfo rev json unmarshal failed")   //======================

		TmpResp := &_resp.VprDeviceReginfoResp{} //======================

		TmpResp.ReceiveTime = time.Now().Format("2006-01-02 15:04:05")
		TmpResp.ErrorMsg = util.DeviceFailInfo99.Msg
		TmpResp.SubCode = util.DeviceFailInfo99.Code

		TmpRespJson, TmpRespRespjsonErr := json.Marshal(TmpResp)
		if TmpRespRespjsonErr != nil {
			log.Infof("南京 VprDeviceReginfo TmpResp send json unmarshal failed")   //======================
			fmt.Println("南京 VprDeviceReginfo TmpResp send json unmarshal failed") //======================
		}
		w.WriteHeader(http.StatusOK)
		w.Write(TmpRespJson)

		return
	}

	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", string(body), e, debug.Stack())
		}
	}()

	fmt.Println("收到http数据：", string(body))
	log.Infof("收到http数据：%v", string(body))

	resp := NjVprServer.VprDeviceReginfo(req) //======================

	respJson, jsonErr := json.Marshal(resp)
	if jsonErr != nil {
		fmt.Println("南京 VprDeviceReginfo send json unmarshal failed") //======================
		log.Infof("南京 VprDeviceReginfo send json unmarshal failed")   //======================
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("发送http数据：", string(respJson))
	log.Infof("发送http数据：%v", string(respJson))

	w.WriteHeader(http.StatusOK)
	w.Write(respJson)

	return
}

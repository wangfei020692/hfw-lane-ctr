package nanjing

import (
	"crypto/md5"
	"fmt"
	log "github.com/cihub/seelog"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

var g_StrNjVersion string = "1.0"
var g_StrNjSignType string = "MD5"

func GetSendDataId() int64 {
	iId := time.Now().Unix()
	return iId
}

func HttpClientPost2Server(strIpAddr, strUrl, reqJson string) string {
	client := &http.Client{}

	strReqUrl := strIpAddr + strUrl

	strLog := "HttpClientPost Send : " + strIpAddr + strUrl + "    data : " + reqJson
	fmt.Println(strLog)
	log.Infof(strLog)

	reqBody := strings.NewReader(reqJson)

	req, err := http.NewRequest("POST", strReqUrl, reqBody)

	//增加header选项
	req.Header.Add("Content-Type", "application/json")

	if err != nil {
		panic(err)
	}

	defer req.Body.Close()

	if err != nil {
		fmt.Println(err)
		log.Infof(err.Error())
		fmt.Println("HttpClientPost remote connect error")
		log.Infof("HttpClientPost remote connect error")
		return "error"
	}

	resp, _ := client.Do(req)
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	strLog = "HttpClientPost Rcv : " + strIpAddr + strUrl + "    data : " + string(body)
	fmt.Println(strLog)
	log.Infof(strLog)

	return string(body)
}

// strSign 请求参数的签名串, strBizContent 业务请求内容
func HttpClientPost2DeviceVprVlpr(strIpAddr, strUrl, reqJson, strSign, strBizContent string) string {
	client := &http.Client{}

	strReqUrl := "http://" + strIpAddr + strUrl

	strLog := "HttpClientPost Send : " + strIpAddr + strUrl + "    data : " + reqJson
	fmt.Println(strLog)
	log.Infof(strLog)

	reqBody := strings.NewReader(reqJson)

	req, err := http.NewRequest("POST", strReqUrl, reqBody)
	if err != nil {
		panic(err)
	}
	//增加header选项
	req.Header.Add("Content-Type", "application/json;charset=UTF-8")

	//业务header
	req.Header.Add("signType", g_StrNjSignType)
	switch g_StrNjSignType {
	case "MD5":
		srcCode := md5.Sum([]byte(strSign))
		code := fmt.Sprintf("%X", srcCode)
		req.Header.Add("sign", code)
		break
	}

	req.Header.Add("timestamp", time.Now().Format("2006-01-02 15:04:05"))
	req.Header.Add("version", g_StrNjVersion)
	req.Header.Add("bizContent", strBizContent)
	//===============================================
	defer req.Body.Close()

	if err != nil {
		fmt.Println(err)
		log.Infof(err.Error())
		fmt.Println("HttpClientPost remote connect error")
		log.Infof("HttpClientPost remote connect error")
		return "error"
	}

	resp, _ := client.Do(req)
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	strLog = "HttpClientPost Rcv : " + strIpAddr + strUrl + "    data : " + string(body)
	fmt.Println(strLog)
	log.Infof(strLog)

	return string(body)
}

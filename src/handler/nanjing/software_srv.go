package nanjing

import (
	"encoding/json"
	_base "hfwLaneCtr/proto/nanjing/software/software_base"
	_req "hfwLaneCtr/proto/nanjing/software/software_req"
	_resp "hfwLaneCtr/proto/nanjing/software/software_resp"
	"hfwLaneCtr/res"
	"hfwLaneCtr/util"
	"runtime"
	"runtime/debug"
	"time"

	log "github.com/cihub/seelog"
)

var (
	NjSoftwareServer = &njSoftwareServer{}
)

type njSoftwareServer struct {
	G_NjAppUrl        string //车道应用所有接口前缀
	G_NjStateInterval int32  //车道设备状态上传时间间隔，默认6s
	G_NjNtpServer     string //ntp服务器ip地址

	G_NjRsuConnStatus0           int32 //0未连接 1连接
	G_NjMpayConnStatus0          int32 //0未连接 1连接
	G_NjWeightConnStatus0        int32 //0未连接 1连接
	G_NjReaderConnStatus0        int32 //0未连接 1连接
	G_NjReaderConnStatus1        int32 //0未连接 1连接
	G_NjReaderConnStatus2        int32 //0未连接 1连接
	G_NjBlusterConnStatus0       int32 //0未连接 1连接
	G_NjVprConnStatus0           int32 //0未连接 1连接
	G_NjVlprConnStatus0          int32 //0未连接 1连接
	G_NjVlprConnStatus1          int32 //0未连接 1连接
	G_NjCardRobotConnStatus0     int32 //0未连接 1连接
	G_NjPrinterConnStatus0       int32 //0未连接 1连接
	G_NjDigitalScreenConnStatus0 int32 //0未连接 1连接
	G_NjDriverConnStatus0        int32 //0未连接 1连接
}

func (srv *njSoftwareServer) UpdateSystemTime() {
	for {
		if srv.G_NjNtpServer != "" {
			switch runtime.GOOS {
			case "windows":
				dataDay, dataHour := GetNtpTime(srv.G_NjNtpServer + ":123")

				SetWindowsSysTime(dataDay)
				SetWindowsSysTime(dataHour)
			case "linux":
				dataDay, dataHour := GetNtpTime(srv.G_NjNtpServer + ":123")

				SetLinuxSysTime(dataDay + " " + dataHour)
			}
		}

		time.Sleep(time.Minute * 5)
	}
}

func (srv *njSoftwareServer) SendDeviceStatus() {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", "SendDeviceStatus", e, debug.Stack())
		}
	}()

	for {
		if srv.G_NjAppUrl == "" {
			//构造设备状态上传（车道应用接口）入参
			serverReq := &_req.UploadStateReq{}
			serverReq.Id = GetSendDataId()

			//天线
			if srv.G_NjRsuConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Rsu = append(serverReq.Data.Rsu, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Rsu = append(serverReq.Data.Rsu, LabelStatusTemp)
			}

			//移动支付
			if srv.G_NjMpayConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Mpay = append(serverReq.Data.Mpay, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Mpay = append(serverReq.Data.Mpay, LabelStatusTemp)
			}
			//称台
			if srv.G_NjWeightConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Weight = append(serverReq.Data.Weight, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Weight = append(serverReq.Data.Weight, LabelStatusTemp)
			}
			//读写器
			if srv.G_NjReaderConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Reader = append(serverReq.Data.Reader, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Reader = append(serverReq.Data.Reader, LabelStatusTemp)
			}

			if srv.G_NjReaderConnStatus1 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 1
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Reader = append(serverReq.Data.Reader, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 1
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Reader = append(serverReq.Data.Reader, LabelStatusTemp)
			}

			if srv.G_NjReaderConnStatus2 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 2
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Reader = append(serverReq.Data.Reader, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 2
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Reader = append(serverReq.Data.Reader, LabelStatusTemp)
			}

			//栏杆机
			if srv.G_NjBlusterConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Bluster = append(serverReq.Data.Bluster, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Bluster = append(serverReq.Data.Bluster, LabelStatusTemp)
			}
			//车牌识别
			if srv.G_NjVprConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Vpr = append(serverReq.Data.Vpr, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Vpr = append(serverReq.Data.Vpr, LabelStatusTemp)
			}
			//车型识别
			if srv.G_NjVlprConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Vlpr = append(serverReq.Data.Vlpr, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Vlpr = append(serverReq.Data.Vlpr, LabelStatusTemp)
			}

			if srv.G_NjVlprConnStatus1 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 1
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Vlpr = append(serverReq.Data.Vlpr, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 1
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Vlpr = append(serverReq.Data.Vlpr, LabelStatusTemp)
			}
			//自助机器人
			if srv.G_NjCardRobotConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.CardRobot = append(serverReq.Data.CardRobot, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.CardRobot = append(serverReq.Data.CardRobot, LabelStatusTemp)
			}
			//打印机
			if srv.G_NjPrinterConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Printer = append(serverReq.Data.Printer, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Printer = append(serverReq.Data.Printer, LabelStatusTemp)
			}
			//数字经济大屏
			if srv.G_NjDigitalScreenConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.DigitalScreen = append(serverReq.Data.DigitalScreen, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.DigitalScreen = append(serverReq.Data.DigitalScreen, LabelStatusTemp)
			}
			//驱动服务
			if srv.G_NjDriverConnStatus0 == 0 {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceFailInfo.Code
				LabelStatusTemp.Msg = util.DeviceFailInfo.Msg
				serverReq.Data.Driver = append(serverReq.Data.Driver, LabelStatusTemp)
			} else {
				LabelStatusTemp := &_base.LabelStatus{}
				LabelStatusTemp.Role = 0
				LabelStatusTemp.Code = util.DeviceSuccInfo.Code
				LabelStatusTemp.Msg = util.DeviceSuccInfo.Msg
				serverReq.Data.Driver = append(serverReq.Data.Driver, LabelStatusTemp)
			}

			respJson, _ := json.Marshal(serverReq)
			strResp := HttpClientPost2Server(srv.G_NjAppUrl, "/device/upload/state", string(respJson))
			if strResp == "error" {
				log.Info("SendDeviceStatus send err")
			}
		}

		if srv.G_NjStateInterval == 0 {
			time.Sleep(time.Second * 6)
		} else {
			time.Sleep(time.Second * time.Duration(srv.G_NjStateInterval))
		}
	}
}

func (srv *njSoftwareServer) SoftwareInit(req *_req.SoftwareInitReq) *_resp.SoftwareInitResp {
	resp := &_resp.SoftwareInitResp{}

	if req.Data.AppUrl == "" || req.Data.NtpServer == "" || req.Data.StateInterval == 0 {
		log.Infof("SoftwareInit 参数错误!")

		resp.Id = req.Id
		resp.Opt = req.Opt
		resp.Data.Devices = nil
		resp.Data.Code = util.ResultFailInfo.Code
		resp.Data.Msg = util.ResultFailInfo.Msg

		return resp
	}

	srv.G_NjAppUrl = req.Data.AppUrl
	srv.G_NjNtpServer = req.Data.NtpServer
	srv.G_NjStateInterval = req.Data.StateInterval

	//构造http返回json ============================================================================
	var devices []string
	//天线 1开启 0关闭
	if res.Config.NjRsuFlag == 1 {
		devices = append(devices, "rsu")
	}
	//移动支付 1开启 0关闭
	if res.Config.NjMpayFlag == 1 {
		devices = append(devices, "mpay")
	}
	//称台 1开启 0关闭
	if res.Config.NjWeightFlag == 1 {
		devices = append(devices, "weight")
	}
	//读写器 1开启 0关闭
	if res.Config.NjReaderFlag == 1 {
		devices = append(devices, "reader")
	}
	//栏杆机 1开启 0关闭
	if res.Config.NjBlusterFlag == 1 {
		devices = append(devices, "bluster")
	}
	//车牌识别 1开启 0关闭
	if res.Config.NjVprFlag == 1 {
		devices = append(devices, "vpr")
	}
	//车型识别 1开启 0关闭
	if res.Config.NjVlprFlag == 1 {
		devices = append(devices, "vlpr")
	}
	//自助机器人 1开启 0关闭
	if res.Config.NjCardRobotFlag == 1 {
		devices = append(devices, "cardRobot")
	}
	//打印机 1开启 0关闭
	if res.Config.NjPrinterFlag == 1 {
		devices = append(devices, "printer")
	}
	//数字经济大屏 1开启 0关闭
	if res.Config.NjDigitalScreenFlag == 1 {
		devices = append(devices, "digitalScreen")
	}
	//驱动服务 1开启 0关闭
	if res.Config.NjDriverFlag == 1 {
		devices = append(devices, "driver")
	}

	resp.Id = req.Id
	resp.Opt = req.Opt
	resp.Data.Devices = devices
	resp.Data.Code = util.ResultSuccInfo.Code
	resp.Data.Msg = util.ResultSuccInfo.Msg

	return resp
}

package nanjing

import (
	"encoding/binary"
	"flag"
	"io"
	"net"
	"os/exec"
	"runtime/debug"
	"time"

	log "github.com/cihub/seelog"
)

const ntpEpochOffset = 2208988800

type packet struct {
	Settings       uint8
	Stratum        uint8
	Poll           int8
	Precision      int8
	RootDelay      uint32
	RootDispersion uint32
	ReferenceID    uint32
	RefTimeSec     uint32
	RefTimeFrac    uint32
	OrigTimeSec    uint32
	OrigTimeFrac   uint32
	RxTimeSec      uint32
	RxTimeFrac     uint32
	TxTimeSec      uint32
	TxTimeFrac     uint32
}

func GetNtpTime(strNtpAddr string) (string, string) {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", strNtpAddr, e, debug.Stack())
		}
	}()

	var host string
	flag.StringVar(&host, "e", strNtpAddr, "NTP host")
	flag.Parse()

	conn, err := net.Dial("udp", host)
	if err != nil {
		log.Info("GetNtpTime failed to connect: ", err)
		return "", ""
	}
	defer conn.Close()
	if err := conn.SetDeadline(time.Now().Add(15 * time.Second)); err != nil {
		log.Info("GetNtpTime failed to set deadline: ", err)
		return "", ""
	}

	req := &packet{Settings: 0x1B}

	if err := binary.Write(conn, binary.BigEndian, req); err != nil {
		log.Infof("GetNtpTime failed to send request: ", err)
		return "", ""
	}

	rsp := &packet{}
	if err := binary.Read(conn, binary.BigEndian, rsp); err != nil {
		log.Infof("GetNtpTime failed to read server response: ", err)
		return "", ""
	}

	secs := float64(rsp.TxTimeSec) - ntpEpochOffset
	nanos := (int64(rsp.TxTimeFrac) * 1e9) >> 32

	dataDay := time.Unix(int64(secs), nanos).Format("2006-01-02")
	dataHour := time.Unix(int64(secs), nanos).Format("15:04:05")
	return dataDay, dataHour
}

func SetWindowsSysTime(dataTime string) {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", "SetWindowsSysTime", e, debug.Stack())
		}
	}()

	cmd := exec.Command("time", dataTime)

	var stdout io.ReadCloser
	var err error
	if stdout, err = cmd.StdoutPipe(); err != nil { //获取输出对象，可以从该对象中读取输出结果
		log.Infof("SetWindowsSysTime 获取输出对象 err:", err)
		return
	}

	defer stdout.Close()                // 保证关闭输出流
	if err := cmd.Start(); err != nil { // 运行命令
		log.Infof("SetWindowsSysTime 运行命令 err:", err)
		return
	}
}

func SetLinuxSysTime(dataTime string) {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", "SetLinuxSysTime", e, debug.Stack())
		}
	}()

	cmd := exec.Command("date", "-s", dataTime)

	var stdout io.ReadCloser
	var err error
	if stdout, err = cmd.StdoutPipe(); err != nil { //获取输出对象，可以从该对象中读取输出结果
		log.Infof("SetLinuxSysTime 获取输出对象 err:", err)
		return
	}

	defer stdout.Close()                // 保证关闭输出流
	if err := cmd.Start(); err != nil { // 运行命令
		log.Infof("SetLinuxSysTime 运行命令 err:", err)
		return
	}
}

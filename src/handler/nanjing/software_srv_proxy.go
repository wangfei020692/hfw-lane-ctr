package nanjing

import (
	"encoding/json"
	"fmt"
	_req "hfwLaneCtr/proto/nanjing/software/software_req"
	_resp "hfwLaneCtr/proto/nanjing/software/software_resp"
	"hfwLaneCtr/util"
	"io/ioutil"
	"net/http"
	"runtime/debug"

	log "github.com/cihub/seelog"
)

var (
	NjSoftwareServerProxy = &njSoftwareServerProxy{}
)

type njSoftwareServerProxy struct {
}

func (srv *njSoftwareServerProxy) SoftwareInit(w http.ResponseWriter, r *http.Request) {
	fmt.Println("接口 南京 SoftwareInit 收到数据") //======================
	log.Infof("接口 南京 SoftwareInit 收到数据")   //======================

	w.Header().Set("Access-Control-Allow-Origin", "*")                                          //允许访问所有域
	w.Header().Add("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-csrftoken") //header的类型 返回数据格式是json
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Max-Age", "0")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("content-type", "application/json;charset=UTF-8")

	defer r.Body.Close()

	var req *_req.SoftwareInitReq //======================
	var err error

	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &req)

	if err != nil {
		fmt.Println("南京 SoftwareInit rev json unmarshal failed") //======================
		log.Infof("南京 SoftwareInit rev json unmarshal failed")   //======================

		TmpResp := &_resp.SoftwareInitResp{} //======================

		TmpResp.Id = GetSendDataId()
		TmpResp.Opt = 1
		TmpResp.Data.Devices = nil
		TmpResp.Data.Code = util.ResultFailInfo.Code
		TmpResp.Data.Msg = util.ResultFailInfo.Msg

		TmpRespJson, TmpRespRespjsonErr := json.Marshal(TmpResp)
		if TmpRespRespjsonErr != nil {
			log.Infof("南京 SoftwareInit TmpResp send json unmarshal failed")   //======================
			fmt.Println("南京 SoftwareInit TmpResp send json unmarshal failed") //======================
		}
		w.WriteHeader(http.StatusOK)
		w.Write(TmpRespJson)

		return
	}

	defer func() {
		if e := recover(); e != nil {
			log.Errorf("%s panic: %s: %s", string(body), e, debug.Stack())
		}
	}()

	fmt.Println("收到http数据：", string(body))
	log.Infof("收到http数据：%v", string(body))

	resp := NjSoftwareServer.SoftwareInit(req) //======================

	respJson, jsonErr := json.Marshal(resp)
	if jsonErr != nil {
		fmt.Println("南京 SoftwareInit send json unmarshal failed") //======================
		log.Infof("南京 SoftwareInit send json unmarshal failed")   //======================
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("发送http数据：", string(respJson))
	log.Infof("发送http数据：%v", string(respJson))

	w.WriteHeader(http.StatusOK)
	w.Write(respJson)

	return
}

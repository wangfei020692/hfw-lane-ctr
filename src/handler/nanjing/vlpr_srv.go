package nanjing

import (
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
	_req "hfwLaneCtr/proto/nanjing/vlpr/vlpr_req"
	_resp "hfwLaneCtr/proto/nanjing/vlpr/vlpr_resp"
	"hfwLaneCtr/res"
	"hfwLaneCtr/util"
)

var (
	NjVlprServer = &njVlprServer{}
)

type njVlprServer struct {
}

const (
	g_VlprOpt_1 = 0x01 //连接
	g_VlprOpt_2 = 0x02 //获取车辆图片、视频
	g_VlprOpt_3 = 0x03 //车型上报
)

func (srv *njVlprServer) VlprApi(req *_req.VlprApiReq) *_resp.VlprApiResp {
	//构造返回
	resp := &_resp.VlprApiResp{}
	resp.Id = req.Id
	resp.Opt = req.Opt
	resp.Data.Code = util.ResultSuccInfo.Code
	resp.Data.Msg = util.ResultSuccInfo.Msg

	switch req.Opt {
	case g_VlprOpt_1: //连接
		go srv.VlprResultConnect(req)
		break
	case g_VlprOpt_2: //获取车辆图片、视频
		break
	case g_VlprOpt_3: //车型上报
		break
	}

	return resp
}

func (srv *njVlprServer) VlprResultConnect(req *_req.VlprApiReq) {
	//参数名ASCII码从小到大排序（字典序）,*******在赋值的时间代码就要写好顺序*******
	//与入口据超一个数据
	devReq := &_req.VlprDeviceInitReq{}
	devReq.DevIP = res.Config.Addr               //必填
	devReq.DevPort = int32(res.Config.HttpPort)  //必填
	devReq.Interval = 300                        //非必填
	devReq.Passwd = res.Config.StrNjVlprPassWord //非必填
	if req.Data.Role == 0 {
		// 0据超 车道口首个车型识别设备
		devReq.Switch = 0x10 //如果是据超 不初始化车型识别
	} else {
		// 1前置 车道口第二个车型识别设备
		devReq.Switch = 0 //前置全开
	}
	//必填 全开
	devReq.User = res.Config.StrNjVlprUserName //非必填

	reqJson, _ := json.Marshal(devReq)

	//创造signValue
	strSignValue := fmt.Sprintf("devIP=%s&devPort=%d", devReq.DevIP, devReq.DevPort)
	if devReq.Interval != 0 {
		strSignValue += fmt.Sprintf("&interval=%d", devReq.Interval)
	}
	if devReq.Passwd != "" {
		strSignValue += fmt.Sprintf("&passwd=%s", devReq.Passwd)
	}
	if req.Data.Role == 0 {
		// 0据超 车道口首个车型识别设备
		strSignValue += "&switch=16" //如果是据超 不初始化车型识别
	} else {
		// 1前置 车道口第二个车型识别设备
		strSignValue += "&switch=0" //前置全开
	}

	if devReq.User != "" {
		strSignValue += fmt.Sprintf("&user=%s", devReq.User)
	}
	strSignValue += "&filename=init.json"

	strDeviceResp := HttpClientPost2DeviceVprVlpr(res.Config.StrNjVlprAddr, "/vtr/init", string(reqJson), strSignValue, "init")
	if strDeviceResp == "error" {
		return
	}

	//需要做返回
	var deviceResp *_resp.VlprDeviceInitResp
	err := json.Unmarshal([]byte(strDeviceResp), &deviceResp)
	if err != nil {
		fmt.Println("南京 VlprResultConnect VlprDeviceInitResp rev json unmarshal failed")
		log.Infof("南京 VlprResultConnect VlprDeviceInitResp rev json unmarshal failed")
		return
	}

	if deviceResp.SubCode == 0 { //成功
		NjSoftwareServer.G_NjVlprConnStatus0 = 1 //状态上报赋值

		reqServer := &_req.VlprResultConnectReq{}
		reqServer.Id = GetSendDataId()
		reqServer.Opt = g_VprOpt_1
		reqServer.Data.Code = util.ResultSuccInfo.Code
		reqServer.Data.Msg = util.ResultSuccInfo.Msg

		reqJson, _ := json.Marshal(reqServer)

		strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vlpr", string(reqJson))
		if strResp == "error" {
			log.Info("VlprResultConnect HttpClientPost2Server send err")
		}
	} else { //失败
		NjSoftwareServer.G_NjVlprConnStatus0 = 0 //状态上报赋值

		reqServer := &_req.VlprResultConnectReq{}
		reqServer.Id = GetSendDataId()
		reqServer.Opt = g_VprOpt_1
		reqServer.Data.Code = util.ResultFailInfo.Code
		reqServer.Data.Msg = util.ResultFailInfo.Msg

		reqJson, _ := json.Marshal(reqServer)

		strResp := HttpClientPost2Server(NjSoftwareServer.G_NjAppUrl, "/device/vlpr", string(reqJson))
		if strResp == "error" {
			log.Info("VlprResultConnect HttpClientPost2Server send err")
		}
	}
}
